package ssii.psi3.freevpn;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Datos de una petición.</p>
 */
class Request {
    // Una cuenta corriente esta formada por una cadena entre 12 y 16 caracteres
    private static final int MAX_CC_LENGTH = 16;
    private static final int MIN_CC_LENGTH = 12;
    // Tamaño máximo de la cantidad de la transacción (desde 999999999999 a 0.0000000001)
    private static final int MAX_SIZE_LENGTH = 12;
    // Tamaño máximo de una línea de transacción (2 códigos de cuenta, 2 espacios y la cantidad)
    private static final int MAX_LINE_LENGTH = MAX_CC_LENGTH*2 + 2 + MAX_SIZE_LENGTH;
    // Una cuenta corriente esta compuesta por caracteres entre A-Z y 0-9.
    private static final Pattern CC_PATTERN = Pattern.compile("^([A-Z0-9])+$");

    String transactionString;
    String fromCc;
    String toCc;
    float size;

    /**
     * <p>Crea una transacción a partir de una línea recibida por el servidor.</p>
     *
     * <p>Lanzará una IllegalArgumentException si aparece algún error durante el parseo
     * de la transacción.</p>
     *
     * @param transactionString Línea representando una transacción.
     */
    Request(String transactionString) {
        if (transactionString.length() > MAX_LINE_LENGTH)
            throw new IllegalArgumentException("Illegal transaction: line is too long (max " + MAX_LINE_LENGTH + ")");
        String[] words = transactionString.split(" ");

        if (words.length != 3)
            throw new IllegalArgumentException("Illegal transaction: 3 strings separated by ' ' expected: " + transactionString);

        float size;
        try { size = Float.parseFloat(words[2]); }
        catch (Exception e) { throw new IllegalArgumentException("Illegal transaction: transaction size is not a float: " + words[2]); }

        init(words[0], words[1], size);
    }

    /**
     * Crea una transacción a partir de la cadena de origen, destino y la cantidad.
     *
     * @param fromCc Cuenta de origen. Debe ser una cadena entre MAX_CC_LENGTH y MIN_CC_LENGTH y compuesta de caracteres A-Z y 0-9.
     * @param toCc Cuenta de destino. Mismos requisitos que el parámetro anterior.
     * @param size Tamaño de la transaferencia. Debe ser mayor que 0.
     */
    Request(String fromCc, String toCc, float size) {
        init(fromCc, toCc, size);
    }

    private void init(String fromCc, String toCc, float size) {
        if (fromCc.length() > MAX_CC_LENGTH || fromCc.length() < MIN_CC_LENGTH)
            throw new IllegalArgumentException("Illegal transaction: FROM_CC has not between " + MIN_CC_LENGTH + " and " + MAX_CC_LENGTH + ": " + fromCc);
        if (!checkAlphabet(fromCc))
            throw new IllegalArgumentException("Illegal transaction: FROM_CC has invalid characters: " + fromCc);

        if (!checkAlphabet(toCc))
            throw new IllegalArgumentException("Illegal transaction: TO_CC has invalid characters: " + toCc);
        if (toCc.length() > MAX_CC_LENGTH || toCc.length() < MIN_CC_LENGTH)
            throw new IllegalArgumentException("Illegal transaction: TO_CC has not between " + MIN_CC_LENGTH + " and " + MAX_CC_LENGTH + ": " + toCc);

        if (size < 0)
            throw new IllegalArgumentException("Illegal transaction: transaction size should be positive and greater than 0: " + this.size);

        this.fromCc = fromCc;
        this.toCc = toCc;
        this.size = size;
        this.transactionString = fromCc + " " + toCc + " " + size;
    }

    private static boolean checkAlphabet(String cc) {
        Matcher m = CC_PATTERN.matcher(cc);
        return m.find();
    }
}
