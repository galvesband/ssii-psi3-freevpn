package ssii.psi3.freevpn;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * <p>Cliente FreeVPN.</p>
 *
 * <p>Esta clase contiene un wrapper fino alrededor de ClientConnection, que implementa
 * el protocolo de comunicación de transferencias. La capa implementa los detalles de
 * SSL como la carga de un almacen como trust-store y la especificación de
 * <i>Cipher Suites</i> seguros.</p>
 */
class FreeVpnClient {
    private static void out(String line) {
        System.out.println(line);
        System.out.flush();
    }
    private static void err(String line) {
        System.err.println(line);
        System.err.flush();
    }

    private String hostname;
    private int port;
    private String[] usableCipherSuites;
    private SSLContext sslContext;

    /**
     * <p>Crea un nuevo cliente.</p>
     *
     * <p>Esto no abre todavía la conexión con el servidor, pero si inicializa el motor SSL y
     * el almacén de certificados.</p>
     *
     * @param hostname Nombre o dirección ip del servidor.
     * @param port Puerto en el que el servidor espera conexiones.
     * @param trustStoreFilePath Path al archivo jks con los certificados en los que el cliente debe confiar.
     * @param trustStorePassword Contraseña del almacén de certificados.
     * @throws IOException No fue posible leer el almacén de certificados
     * @throws UnrecoverableKeyException Clave dañada en almacén de certificados.
     * @throws CertificateException Error durante el procesado del certificado.
     * @throws NoSuchAlgorithmException La plataforma no implementa alguno de los algoritmos necesarios.
     * @throws KeyStoreException Error al procesar el almacén de certificados.
     * @throws KeyManagementException Error al administrar el almacén de certificados.
     */
    FreeVpnClient(String hostname, int port, String trustStoreFilePath, String trustStorePassword) throws IOException, UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        init(hostname, port, trustStoreFilePath, trustStorePassword);
    }

    private void init(String hostname, int port, String trustStoreFilePath, String trustStorePassword) throws IOException, UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        if (port < 1024 || port > 65535)
            throw new IllegalArgumentException("Port should be in the [1024-65535] range.");

        {
            File trustStoreFile = new File(trustStoreFilePath);
            if (!trustStoreFile.exists())
                throw new FileNotFoundException("Trust Store can not be found: " + trustStoreFilePath);
            if (!trustStoreFile.canRead())
                throw new RuntimeException("Trust Store can not be read: " + trustStoreFilePath);
        }

        out("Client: using trustStore: " + trustStoreFilePath + (trustStorePassword != null ? " (using password: yes)" : " (using password: no)"));
        this.hostname = hostname;
        this.port = port;
        this.usableCipherSuites = null;
        this.sslContext = CipherUtil.getSSLContext(trustStoreFilePath, trustStorePassword);
    }

    /**
     * <p>Realiza una petición al servidor FreeVPN.</p>
     *
     * @param transaction Transacción que queremos que el servidor lleve a cabo.
     * @return Cierto si la transacción se hizo con éxito, falso en otro caso.
     * @throws IOException Si hay un error de comunicación.
     */
    boolean doRequest(Request transaction) throws IOException {
        SSLSocket socket = (SSLSocket) sslContext.getSocketFactory().createSocket(hostname, port);
        if (usableCipherSuites == null)
            usableCipherSuites = CipherUtil.supportedCipherSuiteWhiteList();
        // Establecer los Cipher Suites que consideramos seguros
        socket.setEnabledCipherSuites(usableCipherSuites);

        ClientConnection connection = new ClientConnection(socket);
        return connection.sendRequest(transaction);
    }

    /**
     * Punto de entrada de la aplicación de línea de comandos freevpn-client.
     *
     * @param args Argumentos de entrada.
     */
    public static void main(String [] args) {
        if (args.length == 1 && args[0].equals("help")) {
            printHelp();
            return;
        }

        try {
            ClientCommandLineArguments clientArguments = new ClientCommandLineArguments(args);
            // Por defecto, trustStorePassword es null. El constructor establecerá una cadena vacía
            // para indicar que el usuario quiere introducir la contraseña del trust-store.
            if (clientArguments.trustStorePassword != null) {
                Console console = System.console();
                if (console == null)
                    throw new RuntimeException("Error trying to get access to console to ask for password. Are you running this on a console?");
                console.printf("Write the password of the trust store file (" + clientArguments.trustStoreFilePath + ") and press ENTER:\n");
                clientArguments.trustStorePassword = new String(console.readPassword());
            }

            Request request = new Request(clientArguments.request);
            FreeVpnClient client = new FreeVpnClient(
                    clientArguments.hostname,
                    clientArguments.port,
                    clientArguments.trustStoreFilePath,
                    clientArguments.trustStorePassword
            );
            boolean response = client.doRequest(request);
            if (response)
                out("Transaction done!");
            else
                err("Transaction failed!");
        } catch (Exception e) {
            err(e.getMessage());
        }
    }

    private static void printHelp() {
        System.out.println("freevpn-client - a FreeVPN Client for transactions.");
        System.out.println();
        System.out.println("Usage:");
        System.out.println("  freevpn-client [-option=value ...] <from_cc> <to_cc> <transfer-size>");
        System.out.println("  <from_cc>                   Origin account.");
        System.out.println("  <to_cc>                     Destination account.");
        System.out.println("  <transfer-size>             Amount of money to transfer.");
        System.out.println();
        System.out.println("Available options:");
        System.out.println("  -server=<server-name>       Sets the server's name or ip address.");
        System.out.println("  -port=<port>                Sets the port the server is listening.");
        System.out.println("  -trustStore=<file-path>     Path to the 'jks' file with the certificates the client should trust.");
        System.out.println("  -p                          The 'jks' file is protected with a password. The client will ask for it interactively.");
    }

    private static class ClientCommandLineArguments {
        String hostname = "localhost";
        int port = 7070;
        String trustStoreFilePath = System.getProperty("user.dir") + File.separator + "keystore.jks";
        String trustStorePassword = null;
        String request = "default request";

        ClientCommandLineArguments(String[] args) {
            boolean inOptions = true;
            for (String word : args) {
                if (inOptions) {
                    if (word.startsWith("-server="))
                        hostname = word.replace("-server=", "");
                    else if (word.startsWith("-port=")) {
                        try { port = Integer.parseInt(word.replace("-port=", "")); }
                        catch (Exception e) { throw new IllegalArgumentException("Port should be a number: " + word); }
                    }
                    else if (word.startsWith("-trustStore="))
                        trustStoreFilePath = word.replace("-trustStore=", "");
                    else if (word.equals("-p"))
                        trustStorePassword = ""; // Valor por defecto es null. "" indica que el usuario quiere indicar un password
                    else if (word.startsWith("-"))
                        throw new IllegalArgumentException("Unknown option: " + word);
                    else {
                        inOptions = false;
                        request = word;
                    }
                } else {
                    request += " " + word;
                }
            }
        }
    }
}
