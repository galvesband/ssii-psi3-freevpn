package ssii.psi3.freevpn;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>En esta clase esta la implementación de un par de métodos estáticos relacionados con
 * la selección de <i>Cipher Suites</i> y almacenes de claves o certificados en los que se confía.</p>
 */
class CipherUtil {
    /**
     * <p>Lista de <i>Cipher Suites</i> cuya seguridad es aceptable.</p>
     *
     * <p>Los <i>Cipher Suites</i> cuyos identificadores devuelve esta función son considerados
     * suficientemente seguros para transacciones bancarias. En concreto se ha partido de una lista
     * de <i>suites</i> considerados razonablemente seguros y se han aplicado restricciones a
     * las partes de las <i>suites</i> de forma que métodos o algoritmos que se consideran poco
     * seguros son eliminados. En concreto: </p>
     *
     * <ul>
     *     <li>NO <i>SSL</i>. SSL, en cualquiera de sus versiones (1, 2 y 3) presenta vulnerabilidades y debe
     *     ser evitado.</li>
     *
     *     <li>No usar intercambio de claves RSA, usar siempre DHE en alguna de sus variantes. Atacantes que
     *     consigan la clave pueden decodificar transmisiones que hayan guardado previamente. Diffie-Hellman
     *     efímero (DHE) o su variante de curva elíptica (ECDHE) son más apropiados por mantener
     *     <i>Perfect Forward Secrecy</i>, es decir, el descubrimiento de claves utilizadas
     *     actualmente no compromete la seguridad de las claves usadas con anterioridad.</li>
     *
     *     <li>Solo se permiten claves RSA. Se recomienda el uso de claves suficientemente grandes
     *     (2048 bits o más). No se permiten claves DSS/DSA por ser consideradas demasiado débiles.</li>
     *
     *     <li>Se permite cifrado de bloque CBC y GCM. Ha habido problemas con CBC en el pasado, pero
     *     los ataques dependían del uso de alguna versión de SSL, no permitido en esta lista.</li>
     *
     *     <li>Se permite claves simétricas AES de 128 y 256 bits.</li>
     *
     *     <li>Se permite como algoritmo de integridad SHA, SHA256 y SHA384.</li>
     *
     *     <li>Adicionalmente, se permite el uso del cifrador CHACHA20 y el algoritmo de
     *     integridad POLY1305, de reciente diseño y en proceso de implantación; estan considerados
     *     seguros y rápidos.</li>
     * </ul>
     *
     * <p>Esto implica que las llaves usadas en el servidor necesitan evitar el algoritmo DSS/DSA.
     * Puede usar en su lugar RSA.</p>
     *
     * <p>Ver <a href="https://www.owasp.org/index.php/Transport_Layer_Protection_Cheat_Sheet">
     *     TLS Cheat Sheet (OWASP)</a>.</p>
     */
    public static final String[] cipherSuiteWhiteList = new String[] {
            // *_CHACHA20_POLY1305 son 3 o 4 veces más rápidos que los cipher suites existentes.
            //   http://googleonlinesecurity.blogspot.com/2014/04/speeding-up-and-strengthening-https.html
            //"TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305",
            "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305",
            //"TLS_ECDHE_ECDSA_WITH_CHACHA20_SHA",
            "TLS_ECDHE_RSA_WITH_CHACHA20_SHA",

            "TLS_DHE_RSA_WITH_CHACHA20_POLY1305",
            //"TLS_RSA_WITH_CHACHA20_POLY1305",
            "TLS_DHE_RSA_WITH_CHACHA20_SHA",
            //"TLS_RSA_WITH_CHACHA20_SHA",

            // Done with bleeding edge, back to TLS v1.2 and below
            //"TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384",
            "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384",
            //"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256",
            "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",

            "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
            //"TLS_DHE_DSS_WITH_AES_256_GCM_SHA384",
            "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
            //"TLS_DHE_DSS_WITH_AES_128_GCM_SHA256",

            // TLS v1.0 (with some SSLv3 interop)
            "TLS_DHE_RSA_WITH_AES_256_CBC_SHA384",
            //"TLS_DHE_DSS_WITH_AES_256_CBC_SHA256",
            "TLS_DHE_RSA_WITH_AES_128_CBC_SHA",
            //"TLS_DHE_DSS_WITH_AES_128_CBC_SHA",

            //"TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA",
            //"TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA",
            //"SSL_DH_RSA_WITH_3DES_EDE_CBC_SHA",
            //"SSL_DH_DSS_WITH_3DES_EDE_CBC_SHA",

            // No RSA
            // OJO: Hay que generar una clave RSA, no DSA
            //"TLS_RSA_WITH_AES_256_CBC_SHA256",
            //"TLS_RSA_WITH_AES_256_CBC_SHA",
            //"TLS_RSA_WITH_AES_128_CBC_SHA256",
            //"TLS_RSA_WITH_AES_128_CBC_SHA"
    };

    /**
     * Devuelve un array con los Cipher Suites de la lista blanca soportados por la plataforma.
     *
     * @return Array de Cipher Suites soportados por la plataforma.
     */
    static String[] supportedCipherSuiteWhiteList() {
        String[] availableCiphers;

        SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        availableCiphers = factory.getSupportedCipherSuites();
        Arrays.sort(availableCiphers);

        List<String> rvalList = new ArrayList<>();
        for (String whiteListedCipherSuite : cipherSuiteWhiteList) {
            int index = Arrays.binarySearch(availableCiphers, whiteListedCipherSuite);
            if (index >= 0)
                rvalList.add(whiteListedCipherSuite);
        }

        // Permitir renegociación segura, sin downgrades, para evitar poodle
        // En cualquier caso no usamos SSLv3
        // RFC de SCSV: https://tools.ietf.org/html/rfc5746
        rvalList.add("TLS_EMPTY_RENEGOTIATION_INFO_SCSV");

        return rvalList.toArray(new String[0]);
    }

    /**
     * Devuelve un SSLContext con keyStore y trustedStore inicializados a partir de los argumentos de entrada.
     *
     * @param storeFileName Archivo jks
     * @param storePassword Contraseña del archivo jks
     * @return Contexto SSL inicializado con keyStore y trustedStore del archivo indicado.
     * @throws KeyStoreException Error al instanciar la implementación de KeyStore (jks)
     * @throws IOException Error al acceder al almacén
     * @throws CertificateException Error al procesar algún certificado
     * @throws NoSuchAlgorithmException La plataforma no soporta algún algoritmo necesario para procesar el almacén
     * @throws UnrecoverableKeyException Una clave del almacén esta corrupta.
     * @throws KeyManagementException Error durante la creación del contexto SSL y su KeyStore/TrustStore.
     */
    static SSLContext getSSLContext(String storeFileName, String storePassword) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException {
        KeyStore keyStore = KeyStore.getInstance("jks");
        keyStore.load(new FileInputStream(storeFileName), storePassword.toCharArray());
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, storePassword.toCharArray());
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);

        SSLContext context = SSLContext.getInstance("TLS");
        context.init(
                keyManagerFactory.getKeyManagers(),
                trustManagerFactory.getTrustManagers(),
                new SecureRandom()
        );
        context.createSSLEngine();

        return context;
    }
}
