package ssii.psi3.freevpn;

import java.io.*;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>Implementa el protocolo del lado del servidor.</p>
 *
 * <p>El protocolo es muy simple. Tras abrirse la conexión, el servidor espera
 * la petición del cliente y la procesa, enviando una respuesta y cerrando después
 * la conexión. En el proyecto INTRed se soportaban varias peticiones por conexión, pero
 * en esta versión el protocolo ha sido simplificado.</p>
 *
 * <p>Una petición del cliente consiste en una línea de texto con 3 partes separadas por un espacio:</p>
 * <ul>
 *     <li>El código de la cuenta orígen.</li>
 *     <li>El código de la cuenta destino.</li>
 *     <li>La cantidad de dinero a mover.</li>
 * </ul>
 *
 * <p>Un código de cuenta es una cadena de entre 12 y 16 caracteres compuesta por caracteres alfanuméricos
 * (sólo se admiten mayúsculas).</p>
 *
 * <p>El servidor responderá con otra línea indicando un código numérico y una cadena de caracteres.
 * El código numérico será 200 para éxito o 500 para fallo.</p>
 *
 * <p>La implementación por defecto no hace nada con las peticiones salvo imprimirlas en pantalla, por lo que
 * el resultado será siempre 200 salvo que se cree una nueva clase hija que re-implemente el método processRequest.</p>
 *
 * <p>Esta clase implementa la interfaz Runnable, por lo que es indicada para ser utilizada en hilos o thread-pools.
 * El proceso de la conexión no comienza hasta que no se invoca al método run.</p>
 */
class ServerConnection implements Runnable {
    private static final String RESPONSE_STRING = "%CODE% %RESULT%";
    private static final String LOG_STRING = "%IP% %TIMESTAMP% %RESULT% %TRANSACTION%";

    private static void out(String line) {
        System.out.println(line);
        System.out.flush();
    }
    private static void err(String line) {
        System.err.println(line);
        System.err.flush();
    }

    /**
     * <p>Posibles valores del resultado de una transacción/conexión.</p>
     */
    enum Result {
        /**
         * <p>Ha ocurrido una excepción durante el proceso de la conexión.</p>
         *
         * <p>La petición puede haberse llevado a cabo si la excepción ocurrió después
         * de la llamada al método processRequest. Consultar el resultado de la
         * función transactionResult, si es true la petición se llevó a cabo con éxito.</p>
         */
        EXCEPTION_ERROR,
        // Transacción no ha podido llevarse a cabo por lo que sea (no hay dinero, w/e)
        /**
         * <p>La transacción no ha podido ser llevada a cabo.</p>
         *
         * <p>No han ocurrido excepciones durante el proceso, pero la llamada a
         * processRequest devolvió false.</p>
         */
        TRANSACTION_ERROR,
        /**
         * Transacción / conexión correcta.
         */
        OK
    }

    private Socket connection;
    private BufferedWriter log;
    private Exception exception = null;
    // Resultado general
    private Result result;
    // True si la transacción se llevo a cabo
    private boolean transactionResult = false;

    /**
     * Crea una nueva conexión.
     *
     * @param socket Socket de la conexión con el cliente.
     * @param log El log del servidor se imprimirá aquí.
     */
    ServerConnection(Socket socket, BufferedWriter log) {
        this.connection = socket;
        this.log = log;
        this.result = Result.OK;
    }

    /**
     * Devuelve true si ha ocurrido una excepción y/o la transacción no ha podido completarse.
     * @return Falso si la transacción se llevó a cabo y no hubo excepciones, cierto en otro caso.
     */
    boolean hasError() { return result != Result.OK; }

    /**
     * <p>Devuelve la excepción ocurrida durante el proceso de la conexión.</p>
     *
     * <p>Si no ha ocurrido una excepción entonces devolverá null.</p>
     *
     * <p>La transacción puede haberse llevado a cabo con éxito independientemente de
     * si aquí hay o no una excepción. Consulte el valor de transactionResult.</p>
     *
     * @return Excepción ocurrida durante el proceso de la conexión, o null.
     */
    Exception getException() { return exception; }

    /**
     * <p>Devuelve <i>cierto</i> si la transacción se llevo a cabo.</p>
     *
     * <p>La transacción pudo llevarse a cabo a pesar de que ocurriera una excepción. Consulte el
     * valor devuelto por getError y getException además del devuelto por esta función.</p>
     * @return Cierto si la transacción se llevó a cabo.
     */
    boolean transactionResult() { return transactionResult; }

    /**
     * Procesa la conexión.
     */
    public void run() {
        BufferedReader input = null;
        BufferedWriter output = null;

        Request request = null;
        try {
            input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));

            // El parseo y validación ocurren en el constructor de Request.
            try {
                request = new Request(input.readLine());
            } catch (IllegalArgumentException ie) {
                // La línea que nos ha mandado el cliente no es válida.
                sendResponseError(output);
                // Dejamos que el try/catch superior maneje el estado de la conexión
                throw ie;
            }

            // Procesamos la petición
            result = processRequest(request);
            // Dejamos constancia de si la petición ha sido llevada a cabo o no.
            // Si hay una excepción en sendResponse result será EXCEPTION_ERROR y no
            // quedará constancia de que la transacción se llevó a cabo.
            transactionResult = (result == Result.OK);
            // Enviamos respuesta
            sendResponse(result, output);
        } catch (Exception e) {
            exception = e;
            result = Result.EXCEPTION_ERROR;
        } finally {
            try {
                // Ojo, request puede ser null
                logResult(request, result);
            } catch (IOException e) {
                err("Server: error writing to log: " + e.getMessage());
            }
            try {
                if (input != null)
                    input.close();
                if (output != null)
                    output.close();
                if (connection != null)
                    connection.close();
            } catch (IOException ioe) {
                err("Server: Error closing input stream, output stream or socket with client: " + ioe.getMessage());
            }
        }
    }

    private void sendResponseError(BufferedWriter output) throws IOException {
        output.write(RESPONSE_STRING
                .replace("%CODE%", "500")
                .replace("%RESULT%", "INVALID TRANSACTION")
                + "\n"
        );
        output.flush();
    }

    private void sendResponse(Result result, BufferedWriter output) throws IOException {
        String code, resultString;
        switch (result) {
            case TRANSACTION_ERROR:
                code = "500";
                resultString = "TRANSACTION ERROR";
                break;
            case OK:
                code = "200";
                resultString = "OK";
                break;
            default:
                throw new RuntimeException("Unknown result: " + result.toString());
        }

        output.write(RESPONSE_STRING
                .replace("%CODE%", code)
                .replace("%RESULT%", resultString)
                + "\n"
        );
        output.flush();
    }

    private void logResult(Request request, Result result) throws IOException {
        String resultString;
        if (result != null) {
            switch (result) {
                case EXCEPTION_ERROR:
                    resultString = "ERROR(EXCEPTION)(" + this.getException().getMessage() + ")";
                    break;
                case TRANSACTION_ERROR:
                    resultString = "ERROR(TRANSACTION)";
                    break;
                case OK:
                    resultString = "OK";
                    break;
                default:
                    throw new RuntimeException("Unknown result: " + result.toString());
            }
        } else {
            resultString = "null result";
        }

        log.write(LOG_STRING
                .replace("%IP%", connection.getRemoteSocketAddress().toString())
                .replace("%TIMESTAMP%", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .replace("%RESULT%", resultString)
                .replace("%TRANSACTION%", (request != null ? request.transactionString : "-"))
                + "\n"
        );
        log.flush();
    }

    /**
     * <p>Procesa la petición del cliente.</p>
     *
     * <p>La implementación por defecto de este método se limita a imprimir en pantalla la petición
     * y devolver <i>cierto</i>. Es posible reimplementar este método en una clase derivada para
     * hacer algo útil, como una transacción bancaria.</p>
     *
     * @param request Petición del cliente.
     * @return Cierto si la petición ha tenido éxito, falso en otro caso.
     */
    protected Result processRequest(Request request) {
        out("Server: Processed transaction: " + request.fromCc + " " + request.toCc + " " + request.size);
        return Result.OK;
    }
}
