package ssii.psi3.freevpn;

import java.io.*;
import java.net.ProtocolException;
import java.net.Socket;

/**
 * Implementa el protocolo del cliente.
 *
 * @see ServerConnection
 */
class ClientConnection {
    private static void out(String line) {
        System.out.println(line);
        System.out.flush();
    }
    private static void err(String line) {
        System.err.println(line);
        System.err.flush();
    }

    private static class ResponseData {
        int code = 0;
        String response = "";
    }

    private Socket socket;
    private BufferedReader input = null;
    private BufferedWriter output = null;

    /**
     * Crea un nuevo cliente.
     *
     * @param socket Conexión con el servidor.
     * @throws IOException Error al intentar crear los flujos de entrada y salida de la conexión.
     */
    ClientConnection(Socket socket) throws IOException {
        this.socket = socket;

        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (Exception e) {
            if (input != null) {
                input.close();
                input = null;
            }
            if (output != null) {
                output.close();
                output = null;
            }
            if (socket != null && socket.isConnected())
                socket.close();

            throw e;
        }
    }

    /**
     * <p>Envía una petición al servidor y recibe la respuesta, indicando el resultado en el valor de retorno.</p>
     *
     * @param transaction Transacción que se desean enviar al servidor.
     * @return Cierto, si el servidor ha indicado que la petición se ha procesado correctamente. Falso en otro caso.
     * @throws IOException Si hay cualquier problema en la comunicación con el servidor.
     */
    boolean sendRequest(Request transaction) throws IOException {
        if (input == null || output == null || socket == null || socket.isClosed())
            throw new RuntimeException("Error attempting to send a request: connection is closed.");

        try {
            output.write(transaction.transactionString + "\n");
            output.flush();

            ResponseData response = receiveResponse(input);
            out("Response " + (response.code == 200 ? "OK: " : "ERROR: ") + response.response);

            // Esperamos a que el servidor cierre conexión
            String nextLine = input.readLine();
            if (nextLine != null)
                throw new ProtocolException("Expected server closing connection.");

            return (response.code == 200);
        } catch (IOException ioe) {
            try {
                close();
            } catch (Exception e2) {}

            throw ioe;
        } finally {
            close();
        }
    }

    /**
     * Cierra la conexión con el servidor si aún esta abierta.
     *
     * @throws IOException Error al intentar cerrar los flujos de entrada o salida de la conexión o la conexión misma.
     */
    void close() throws IOException {
        if (input != null) {
            input.close();
            input = null;
        }
        if (output != null) {
            output.close();
            output = null;
        }
        if (socket != null) {
            if (!socket.isClosed()) socket.close();
            socket = null;
        }
    }

    private static ResponseData receiveResponse(BufferedReader input) throws IOException {
        ResponseData rval = new ResponseData();

        String message = input.readLine();
        if (message == null)
            throw new EOFException("Server closed connection unexpectedly.");
        String[] messageWords = message.split(" ");

        switch (messageWords[0]) {
            case "200":
                rval.code = 200;
                break;
            case "500":
                rval.code = 500;
                break;
            default:
                throw new ProtocolException("Unknown code in response: " + messageWords[0]);
        }

        for (int i = 1; i < messageWords.length; i++)
            rval.response += " " + messageWords[i];

        return rval;
    }
}
