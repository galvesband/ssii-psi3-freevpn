package ssii.psi3.freevpn;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;
import java.io.*;
import java.net.SocketException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * <p>Servidor FreeVPN.</p>
 *
 * <p>Implementación simple de un servidor FreeVPN. Solo soporta una única conexión
 * concurrente.</p>
 */
class FreeVpnServer implements Runnable {
    private static void log(String line) {
        System.out.println(line);
        System.out.flush();
    }
    private static void err(String line) {
        System.err.println(line);
        System.err.flush();
    }

    private SSLServerSocket serverSocket;
    private String logFilePath = System.getProperty("user.dir") + File.separator + "server.log";

    /**
     * Crea un nuevo servidor FreeVPN.
     *
     * @param keyStoreFilePath Ruta al archivo con el certificado y clave privada.
     * @param keyStorePassword Contraseña del almacen de certificados.
     * @param port Puerto en el que escuchar conexiones.
     * @throws IOException Error al cargar el almacén de claves.
     * @throws UnrecoverableKeyException Clave corrupta.
     * @throws CertificateException Error al procesar el certificado.
     * @throws NoSuchAlgorithmException La plataforma no implementa algún algoritmo necesitado por la clave.
     * @throws KeyStoreException Error durante la lectura del almacén de certificados.
     * @throws KeyManagementException Error administrando las claves del almacén.
     */
    FreeVpnServer(String keyStoreFilePath, String keyStorePassword, int port) throws IOException, UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        init(keyStoreFilePath, keyStorePassword, port);
    }

    private void init(String keyStoreFilePath, String keyPassword, int port) throws IOException, UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        if (port < 1024 || port > 65535) {
            throw new IllegalArgumentException("Invalid port number: should be in [1024-65535] range.");
        }
        {
            File keyStoreFile = new File(keyStoreFilePath);
            if (!keyStoreFile.exists())
                throw new FileNotFoundException("Key Store can not be found: " + keyStoreFilePath);
            if (!keyStoreFile.canRead())
                throw new RuntimeException("Key Store can not be read: " + keyStoreFilePath);
        }

        log("Server: Using keyStore: " + keyStoreFilePath + (keyPassword != null ? " (using password: yes)" : " (using password: no)"));
        ServerSocketFactory socketFactory = CipherUtil.getSSLContext(keyStoreFilePath, keyPassword).getServerSocketFactory();
        serverSocket = (SSLServerSocket) socketFactory.createServerSocket(port);
        // Establecer los Cipher Suites que consideramos seguros
        serverSocket.setEnabledCipherSuites(CipherUtil.supportedCipherSuiteWhiteList());
    }

    /**
     * Devuelve la rutas del archivo de log.
     *
     * @return Ruta del archivo de log utilizado.
     */
    String getLogFilePath() { return this.logFilePath; }

    /**
     * <p>Establece la ruta al archivo de log.</p>
     *
     * <p>Hay que llamar a este método antes de invocar a <i>run()</i>
     * o no tendrá efecto.</p>
     *
     * @param path Ruta al archivo de log.
     */
    void setLogFilePath(String path) { this.logFilePath = path; }

    /**
     * <p>Abre el puerto de escucha y comienza a procesar conexiones.</p>
     *
     * <p>Esta implementación solo soporta una conexión concurrente.</p>
     */
    @Override
    public void run() {
        BufferedWriter logWriter;
        try {
            logWriter = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(new File(logFilePath), true))
            );
            log("Server: Log file: " + logFilePath);
        } catch (FileNotFoundException fnfe) {
            err("Server: Can't create/open log file: " + logFilePath);
            return;
        }

        try {
            while (true) {
                SSLSocket socket = null;

                try {
                    log("Server: Waiting for client connections...");
                    try {
                        socket = (SSLSocket) serverSocket.accept();
                        //socket.setEnabledCipherSuites(CipherUtil.supportedCipherSuiteWhiteList());
                    } catch (SocketException se) {
                        // Socket de servidor cerrado
                        break;
                    }

                    ServerConnection connection = new ServerConnection(socket, logWriter);
                    connection.run();
                    if (connection.getException() != null)
                        throw connection.getException();
                } catch (Exception e) {
                    err("Server: Error while processing a connection: " + e.getMessage());
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (IOException e1) {
                            err("Server: Error closing connection: " + e1.getMessage());
                        }
                    }
                }
            }
        } finally {
            try {
                if (!serverSocket.isClosed())
                    serverSocket.close();
            } catch (Exception e) {
                err("Server: Error closing server socket: " + e.getMessage());
            }
            try {
                logWriter.close();
            } catch (Exception e) {
                err("Server: Error closing log file: " + e.getMessage());
            }
        }
    }

    /**
     * <p>Cierra el servidor.</p>
     *
     * <p>Una conexión actual continuará ejecutándose hasta terminar, pero no se aceptarán
     * más conexiones.</p>
     *
     * @throws IOException Error al cerrar el socket.
     */
    void close() throws IOException {
        if (!serverSocket.isClosed())
            serverSocket.close();
    }

    /**
     * Punto de entrada de la aplicación del servidor freevpn-server.
     *
     * @param args Argumentos de entrada.
     * @throws IOException Error de entrada salida al acceder a la consola o el almacén de claves.
     */
    public static void main(String [] args) throws IOException {
        try {
            if (args.length == 1 && args[0].equals("help")) {
                printHelp();
                return;
            }

            ServerCommandLineArguments serverArgs = new ServerCommandLineArguments(args);
            if (serverArgs.keyStorePassword != null) {
                Console console = System.console();
                if (console == null)
                    throw new RuntimeException("Error trying to get access to console to ask for password. Are you running this on a console?");
                console.printf("Write the password of the key store file (" + serverArgs.keyStoreFilePath + ") and press ENTER: \n");
                serverArgs.keyStorePassword = new String(console.readPassword());
            }

            FreeVpnServer server = new FreeVpnServer(
                    serverArgs.keyStoreFilePath,
                    serverArgs.keyStorePassword,
                    serverArgs.port
            );
            server.setLogFilePath(serverArgs.logFilePath);
            server.run();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void printHelp() {
        System.out.println("freevpn-server - a FreeVPN Server for transactions.");
        System.out.println();
        System.out.println("Usage:");
        System.out.println("  freevpn-server [-option=value ...]");
        System.out.println();
        System.out.println("Available options:");
        System.out.println("  -port=<port>                Port the server will listen connection from.");
        System.out.println("  -keyStore=<file-path>       Path to the jks file with the private key.");
        System.out.println("  -p                          The key store has a password. The server will ask interactively for a password.");
        System.out.println("  -logFile=<file-path>        Path to the server's log file.");
    }

    private static class ServerCommandLineArguments {
        int port = 7070;
        String keyStoreFilePath = System.getProperty("user.dir") + File.separator + "keystore.jks";
        String keyStorePassword = null;
        String logFilePath = System.getProperty("user.dir") + File.separator + "server.log";

        ServerCommandLineArguments(String[] args) {
            for (String pairKeyValue : args) {
                if (pairKeyValue.startsWith("-port=")) {
                    try { port = Integer.parseInt(pairKeyValue.replace("-port=", "")); }
                    catch (Exception e) { throw new IllegalArgumentException("Port should be a number: " + pairKeyValue); }
                }
                else if (pairKeyValue.startsWith("-keyStore=")) {
                    keyStoreFilePath = pairKeyValue.replace("-keyStore=", "");
                }
                else if (pairKeyValue.equals("-p")) {
                    keyStorePassword = ""; // Valor por defecto es null. "" indica que el usuario quiere introducir la contraseña.
                }
                else if (pairKeyValue.startsWith("-logFile="))
                    logFilePath = pairKeyValue.replace("-logFile=", "");
                else
                    throw new IllegalArgumentException("Unknown option: " + pairKeyValue);
            }
        }
    }
}
