package ssii.psi3.freevpn;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

public class CipherUtilTest {
    @Test
    public void supportedCipherSuiteWhiteList() throws Exception {
        String [] cipherSuites = CipherUtil.supportedCipherSuiteWhiteList();

        for (String cipherSuite : cipherSuites) {
            // Sacar por pantalla, por ver los soportados en plataforma actual
            System.out.println("Cipher Suite: " + cipherSuite);

            // No SSLvX
            assertFalse(cipherSuite.startsWith("SSL_"));
            // No intercambio de claves RSA
            assertFalse(cipherSuite.substring(4, 5).equals("RSA"));

            String [] words = cipherSuite.split("_");
            for (String word : words) {
                assertNotEquals(word, "DSS");
                assertNotEquals(word, "DSA");
            }
        }
    }

}