package ssii.psi3.freevpn;

import org.junit.Test;

import javax.net.ssl.SSLHandshakeException;
import java.io.File;

import static org.junit.Assert.*;

public class FunctionalTest {
    // Keystore con el certificado completo (clave pública y privada), para el servidor
    private static final String serverStorePath = System.getProperty("user.dir") + File.separator + "server-keystore.jks";
    private static final String serverStorePassword = "w4uUppBJ";
    // Keystore con la clave pública del certificado del servidor, para el cliente
    private static final String clientStorePath = System.getProperty("user.dir") + File.separator + "client-keystore.jks";
    private static final String clientStorePassword = "lolailo";
    // Keystore altenativo, no tiene ninguna de las dos claves.
    private static final String alternativeStorePath = System.getProperty("user.dir") + File.separator + "empty-keystore.jks";
    private static final String getAlternativeStorePassword = "lerele";

    @Test
    public void validFunctionalTest() throws Exception {
        FreeVpnClient client;
        FreeVpnServer server = null;
        Thread serverThread = null;

        try {
            client = new FreeVpnClient("localhost", 7070, clientStorePath, clientStorePassword);
            server = new FreeVpnServer(serverStorePath, serverStorePassword, 7070);
            serverThread = new Thread(server);
            serverThread.start();

            // Transacción válida
            assertTrue(client.doRequest(new Request("AAAAAAAAAAAAAA BBBBBBBBBBBBBB 100.0")));
        } finally {
            if (server != null)
                server.close();
            if (serverThread != null) {
                serverThread.join();
            }
        }
    }

    @Test
    public void requestValidationFunctionalTest() throws Exception {
        FreeVpnClient client;
        FreeVpnServer server = null;
        Thread serverThread = null;

        try {
            client = new FreeVpnClient("localhost", 7070, clientStorePath, clientStorePassword);
            server = new FreeVpnServer(serverStorePath, serverStorePassword, 7070);
            serverThread = new Thread(server);
            serverThread.start();

            // Crear transacciones con transactionString inválido.
            // El cliente enviará lo que contenga esa línea, aunque haya cambiado desde que validó la entrada.
            Request invalidRequest = new Request("AAAAAAAAAAAA BBBBBBBBBBBBBB 100.0");
            // Carácter inválido FROM_CC
            invalidRequest.transactionString = "AAA;AAAAAAAAA BBBBBBBBBBBBBB 100.0";
            assertFalse(client.doRequest(invalidRequest));

            // Carácter inválido TO_CC
            invalidRequest.transactionString = "AAAAAAAAAAAA BBBBBBBBBBBB;B 100.0";
            assertFalse(client.doRequest(invalidRequest));

            // Carácter inválido en cantidad
            invalidRequest.transactionString = "AAAAAAAAAAAA BBBBBBBBBBBBBB 10a0.0";
            assertFalse(client.doRequest(invalidRequest));

            // FROM_CC muy corto
            invalidRequest.transactionString = "A BBBBBBBBBBBBBB 100.0";
            assertFalse(client.doRequest(invalidRequest));

            // TO_CC muy corto
            invalidRequest.transactionString = "AAAAAAAAAAAAAA B 100.0";
            assertFalse(client.doRequest(invalidRequest));

            // Línea muy larga
            invalidRequest.transactionString = "12345678901234567890123456789012345678901234568790"; // 50 chars
            assertFalse(client.doRequest(invalidRequest));
        } finally {
            if (server != null)
                server.close();
            if (serverThread != null) {
                serverThread.join();
            }
        }
    }

    @Test
    public void clientMissingPublicKeyFunctionalTest() throws Exception {
        FreeVpnClient client;
        FreeVpnServer server = null;
        Thread serverThread = null;

        try {
            // TrustStore del cliente no tiene clave pública de servidor
            client = new FreeVpnClient("localhost", 7070, alternativeStorePath, getAlternativeStorePassword);
            server = new FreeVpnServer(serverStorePath, serverStorePassword, 7070);
            serverThread = new Thread(server);
            serverThread.start();

            client.doRequest(new Request("AAAAAAAAAAAAAA BBBBBBBBBBBBBB 100.0"));
            fail();
        } catch (SSLHandshakeException she) {
            System.out.println(she.getMessage());
        } finally {
            if (server != null)
                server.close();
            if (serverThread != null) {
                serverThread.join();
            }
        }
    }
}
