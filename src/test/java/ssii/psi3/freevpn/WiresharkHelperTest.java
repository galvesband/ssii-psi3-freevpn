package ssii.psi3.freevpn;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class WiresharkHelperTest {
    private static final String storePath = System.getProperty("user.dir") + File.separator + "keystore.jks";
    private static final String storePassword = "w4uUppBJ";

    @Test
    public void doubleEqualTransfer() throws Exception {
        FreeVpnClient client;
        FreeVpnServer server = null;
        Thread serverThread = null;

        try {
            client = new FreeVpnClient("localhost", 7070, storePath, storePassword);
            server = new FreeVpnServer(storePath, storePassword, 7070);
            serverThread = new Thread(server);
            serverThread.start();

            /*
             * El objetivo es hacer 2 transferencias iguales y observar el tráfico a través de wireshark.
             */

            assertTrue(client.doRequest(new Request("AAAAAAAAAAAAAA BBBBBBBBBBBBBB 100.0")));
            assertTrue(client.doRequest(new Request("AAAAAAAAAAAAAA BBBBBBBBBBBBBB 100.0")));
        } finally {
            if (server != null)
                server.close();
            if (serverThread != null) {
                serverThread.join();
            }
        }
    }
}
