package ssii.psi3.freevpn;

import org.junit.Test;

import static org.junit.Assert.*;

public class RequestTest {
    @Test
    public void constructorLineTest() {
        // Request válido
        Request r1 = new Request("0981230A809E2908 098A230980T1C908 100.5");
        assertEquals("0981230A809E2908", r1.fromCc);
        assertEquals("098A230980T1C908", r1.toCc);
        assertTrue(100.5f == r1.size);

        // Número de palabras incorrecto
        try {
            new Request("0981230AS09E2908 098A230980T1C908 100.0 A");
            fail();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        // Float inválido
        try {
            new Request("0981230AS09E2908 098A230980T1C908 100a");
            fail();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        // Cadena muy larga
        try {
            new Request("0123456789012345678901234567890123456789012345678901234568790");
            fail();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void constructorParamsTest() {
        // Request válido
        Request r1 = new Request("0981230AS09E2908", "0981230AS09E2909", 100.5f);
        assertEquals("0981230AS09E2908", r1.fromCc);
        assertEquals("0981230AS09E2909", r1.toCc);
        assertTrue(100.5f == r1.size);

        // From muy pequeño
        try {
            new Request("0988", "0981230AS09E2909", 100.5f);
            fail();
        } catch (IllegalArgumentException e) {}

        // From muy grande
        try {
            new Request("0988AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "0981230AS09E2909", 100.5f);
            fail();
        } catch (IllegalArgumentException e) {}

        // Caracter inválido from
        try {
            new Request("AAAAAAAAAAAA;", "BBBBBBBBBBBBBB", 10f);
            fail();
        } catch (IllegalArgumentException e) {}

        // Caracter inválido to
        try {
            new Request("AAAAAAAAAAAAA", "BBBBBBBBBBBB;B", 10f);
            fail();
        } catch (IllegalArgumentException e) {}

        // To muy pequeño
        try {
            new Request("0988AAAAAAAAAA", "E2909", 100.5f);
            fail();
        } catch (IllegalArgumentException e) {}

        // To muy grande
        try {
            new Request("0988AAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAA0981230AS09E2909", 100.5f);
            fail();
        } catch (IllegalArgumentException e) {}

        // Tamaño negativo
        try {
            new Request("AAAAAAAAAAAAAA", "BBBBBBBBBBBBBB", -10f);
            fail();
        } catch(IllegalArgumentException e) {}
    }
}